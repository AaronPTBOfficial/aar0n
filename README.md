# aar0n

A very silly Portal mod where different resources are replaced with AaronPTB. This repository was made to make the development process faster so that it ends up getting finished faster and is more likely to get finished, which it very well deserves

## To-do

### Textures
- [x] Remake elevator texture
- [ ] Finish signage
- [x] Replace Chell's face texture with Aaron
- [x] Remake door texture

### Sound
- [ ] Entirely new line for usetoilet_thank
- [x] Remake original Portal radio loop with Aaron sounds
- [ ] Finish GLaDOS lines
- [x] Replace turret gun sounds
- [ ] Replace turret knife fight line